{ pkgs ? import <nixpkgs> {}, ... }: pkgs.stdenv.mkDerivation rec {
  pname = "tachikoma-fe";
  version = "develop";

  src = ./.;

  nativeBuildInputs = with pkgs; [
    prefetch-yarn-deps
    yarn
    nodejs
    jpegoptim
    oxipng
    nodePackages.svgo
  ];

  offlineCache = pkgs.fetchYarnDeps {
    yarnLock = src + "/yarn.lock";
    hash = "sha256-rzBGji5qZ6BUyeJjawfSVRZIgTGXDMBtZL6Qp9EkBnI=";
  };

  postPatch = ''
    # Build scripts assume to be used within a Git repository checkout
    sed -E -i '/^let commitHash =/,/;$/clet commitHash = "develop";' \
      build/webpack.prod.conf.js
  '';

  configurePhase = ''
    runHook preConfigure

    export HOME=$(mktemp -d)

    yarn config --offline set yarn-offline-mirror ${pkgs.lib.escapeShellArg offlineCache}
    fixup-yarn-lock yarn.lock

    yarn install --offline --frozen-lockfile --ignore-scripts --no-progress --non-interactive

    runHook postConfigure
  '';

  buildPhase = ''
    runHook preBuild

    export NODE_ENV="production"
    export NODE_OPTIONS="--openssl-legacy-provider"
    yarn run build --offline

    runHook postBuild
  '';

  installPhase = ''
    runHook preInstall

        # (Losslessly) optimise compression of image artifacts
    find dist -type f -name '*.jpg' -execdir ${pkgs.jpegoptim}/bin/jpegoptim -w$NIX_BUILD_CORES {} \;
    find dist -type f -name '*.png' -execdir ${pkgs.oxipng}/bin/oxipng -o max -t $NIX_BUILD_CORES {} \;
    find dist -type f -name '*.svg' -execdir ${pkgs.nodePackages.svgo}/bin/svgo {} \;

    cp -R -v dist $out

    runHook postInstall
  '';

}
